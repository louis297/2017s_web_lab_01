* Herbivorous:
.. Horse
.. Cow
.. Sheep
.. Capybara

Carnivorous:
.. Cat
.. Polar Bear
.. Frog
.. Hawk
.. Lion
.. Raccoon

Omnivorous:
.. Dog
.. Rat
.. Fox
.. Chicken
.. Fennec fox

